//
//  RealmChanges.swift
//  Vkontakte-GB-Result
//
//  Created by Danil Darskiy on 06.06.2019Thursday.
//  Copyright © 2019 Danil Darskiy-GB-Result. All rights reserved.
//

import Foundation
import UIKit

extension IndexPath {
    static func fromRow(_ row: Int) -> IndexPath {
        return IndexPath(row: row, section: 0)
    }
}

extension UITableView {
    func applyChanges(deletions: [Int], insertions: [Int], updates: [Int]) {
        beginUpdates()
        deleteRows(at: deletions.map(IndexPath.fromRow), with: .automatic)
        insertRows(at: insertions.map(IndexPath.fromRow), with: .automatic)
        reloadRows(at: updates.map(IndexPath.fromRow), with: .automatic)
        endUpdates()
    }
}

//extension UICollectionView {
//    func applyChanges(deletions: [Int], insertions: [Int], updates: [Int]) {
//        Update()
//        deleteRows(at: deletions.map(IndexPath.fromRow), with: .automatic)
//        insertRows(at: insertions.map(IndexPath.fromRow), with: .automatic)
//        reloadRows(at: updates.map(IndexPath.fromRow), with: .automatic)
//        endUpdates()
//    }
//}
